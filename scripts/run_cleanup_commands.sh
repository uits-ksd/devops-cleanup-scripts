#!/bin/bash

inputFile=${1?Error: no file provided}

while IFS= read -r line
do
  echo $line
  
  if [[ $line = *"rm -rf"* ]]; then
    eval "$line"
  fi
done < $inputFile