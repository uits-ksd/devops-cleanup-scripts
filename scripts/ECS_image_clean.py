#!/usr/bin/python3

import sys
import boto3
import dateutil.parser
import dateutil.tz
import re
import os
from datetime import *

# use this to find python version running this script (for troubleshooting)
# print(sys.version)

regionname='us-west-2'

# get todays date
today = datetime.now(timezone.utc)

# date calculations come from https://jira.arizona.edu/browse/UAF-6343
# calculate 30 days ago for the   daily - 30 days
daily_age_to_delete = (today - timedelta(days=30))

# calculate one year ago for the release - 365 days
release_age_to_delete = (today - timedelta(days=365))

prototype_build_tags_to_ignore = "SNAPSHOT-UAF"  # if we just ignore this tag, the daily_build_list will have all we need.
release_build_tags = "ua-release" 

# set up our ECR config.  This is tied into the account configured in aws configure so it may be different if you run locally
ecr = boto3.client('ecr', region_name='us-west-2')

# DRYRUN is a param passed in by Jenkins, the if check below in here is so it can be run locally if needed.
# DRYUN = False means OMG WE ARE DELETING THINGS. NOTE: There is no comming back from ECR deletions as of 6/22/18
if "DRYRUN" in os.environ:
  DRYRUN = os.environ['DRYRUN']  
else:  
  DRYRUN = True  # always be true for local runs...change this manually if you want to delete while testing

# this function will get a list of the images in the repo we specify
def describe_images(repoName):     
   token = None

   while True:
    # A token to specify where to start paginating for AWS.
    if token:
      res = ecr.describe_images(repositoryName=repoName, nextToken=token)
    else:
      res = ecr.describe_images(repositoryName=repoName)

    for img in res.get('imageDetails'):
      yield img  # There can be a LOT Of images, so only keep it once

    token = res.get('nextToken')
    if not token:
      break
  
# This function will build the 'daily image list' for us  
def get_daily_build_list(repoName):
  print("reponame is " , repoName)
  print("\n -------GETTING DAILY BUILD IMAGE LIST of %s-----------\n" % repoName)
  daily_build_list = []

  # we need to search the list of images that have a tag for daily builds  
  # do a search in image tags for anything that matches the daily build tags  
  # describe_images is a function up above somewhere
  for img in describe_images(repoName):
    if img.get('imageTags') is not None:
      for tag in img.get('imageTags'):
        # NOTE:  re.search is a python regex function we get from import re    
        if (not (re.search(prototype_build_tags_to_ignore, tag))) and (not (re.search('master-', tag))):
          # print(tag)
          daily_build_list.append(img)
 
  print("Number of daily build list in ", repoName, " = ", len(daily_build_list)) 
  return daily_build_list
  
# Now that we have a list of Daily Builds
# This function will search our 'daily images list' for the age range we have specified.
# see variable at top of script    
def daily_build_age_search(daily_builds):
  print("\n -------SEARCHING DAILY BUILD LIST FOR AGE------- \n")

  # make a list to hold what images we want to delete if they match the age range
  daily_images_to_delete = []

  print("\nNumber of daily images(sanity check) = " , len(daily_builds))
  print("Daily tags: ", prototype_build_tags_to_ignore )
  
  for image in daily_builds:
    date_image_pushed = image.get('imagePushedAt')    

    # check to see if the date the image was pushed is older than daily build age range
    if (date_image_pushed <= daily_age_to_delete):      
      daily_images_to_delete.append(image)
      print("Image to be deleted: ", image.get('imageTags'), "Date Pushed: ", image.get('imagePushedAt'))
  
  print("daily_images_to_delete = ", len(daily_images_to_delete), "\n")    
  return daily_images_to_delete
    
# similar to daily build list, but gets the 'release build images'    
def get_release_build_list(repoName):
  print("\n ------GETTING RELEASE BUILD LIST for %s------ \n" % repoName)
  
  # make a list to hold our 'release images' that match the release tag
  release_build_list = []

  # we need to search the list of images that have a tag for release images
  # do a search in image tags for anything that matches the release build tags
  for img in describe_images(repoName):    
    if img.get('imageTags') is not None:
      for tag in img.get('imageTags'):
        # NOTE:  re.search is a python regex function we get from import re
        if (re.search(release_build_tags, tag)):          
          release_build_list.append(img)
  
  print("Number of release build list = ", len(release_build_list)) 
  #print(get_release_build_list)
  return release_build_list

# do a search on the release build list to find images older than the release age range
def release_age_search(release_builds):
  print("\n ------SEARCHING RELEASE BUILD LIST FOR AGE----- \n")
       
  # make a list of images fore release that are older than 365 days  
  release_images_to_delete = []

  # sanity check, this 'should' match the number in the release build list
  print("Number of release build list(sanity check) = " , len(release_builds))  
  print("Release tags: ", release_build_tags )
  
  for image in release_builds:       
    date_image_pushed = image.get('imagePushedAt')       
    if (date_image_pushed <= release_age_to_delete):      
      release_images_to_delete.append(image)
      print("Image to be deleted: ", image.get('imageTags'), "Date Pushed: ", image.get('imagePushedAt'))
  
  print("release_images_to_delete = ", len(release_images_to_delete))
  return release_images_to_delete  

# this is used to chunk the images for large lists and used in delete_images
def chunks(l, n=100):
  for i in range(0, len(l), n):
    yield l[i:i + n]

# function used to create the proper list for batch_delete_image (aws function)
def append_to_list(list, id):
    if not {'imageDigest': id} in list:
        list.append({'imageDigest': id})    

# once we have all of our images parsed for tags and age, we can now delete them
# NOTE: The DRYRUN boolean determines if it actually deletes or just prints
def delete_images(images_to_delete, repo):
      print("\n ------DELETING IMAGES----- \n")
      print("length of images to delete: ", len(images_to_delete), "\n")

      # create a list that has "imageDigest": "sha256:<sha number goes here>"
      deletesha = []  

      for item in images_to_delete:      
        registryId = item.get('registryId')
        append_to_list(deletesha, item['imageDigest'])

        # START loop to delete the image, but do it in chunks
        # Chunking is what AWS recommends 
        i = 0        
        for deleteImage in chunks(images_to_delete, 100):             
              i += 1
              # Set DRYRUN to FALSE at the top of this script to actually delete images.
              if DRYRUN == "False":                  
                  delete_response = ecr.batch_delete_image(
                      registryId=registryId,
                      repositoryName=repo,
                      imageIds=deletesha
                  )
                  print(delete_response)
              # else:  # this can be uncommented for more testing if needed.                 
              #     # print("PRETENDING TO DELETE ")
              #     print(registryId)
              #     print("registryId:" + registryId)
              #     print("repositoryName:" + repo)
              #     print("Deleting {} chunk of images".format(i))
              #     print("imageIds:", end='')
              #     print(deleteImage)

def main():

    #Logic:  
    #  1. Get a list of 'daily build' images from the kfs7 repo.  
    #  2. Delete them
    #  3. Get a list of 'release build' images from the kfs7 repo
    #  3. Delete them
    #  4. Rinse/repeat for the kuali/rice repo.
    
    kuali_repo = "kuali/kfs7"
    rice_repo =  "kuali/rice"

    # ------ KFS DAILY IMAGES ----------
        
    kfs_daily_images = get_daily_build_list(kuali_repo)    
    kfs_daily_images_to_delete = daily_build_age_search(kfs_daily_images)
    print("KFS daily images to delete: " , len(kfs_daily_images_to_delete))
    delete_images(kfs_daily_images_to_delete,kuali_repo )

    # ----------- KFS RELEASE IMAGES------------------

    kfs_release_builds = get_release_build_list(kuali_repo)
    kfs_release_images_to_delete = release_age_search(kfs_release_builds)
    print("kfs_release_images_to_delete", len(kfs_release_images_to_delete) )
    delete_images(kfs_release_images_to_delete, kuali_repo)
        
    #     # ------ RICE DAILY IMAGES -------
    rice_daily_images = get_daily_build_list(rice_repo)
    rice_daily_images_to_delete = daily_build_age_search(rice_daily_images)
    print("RICE daily images to delete: " , len(rice_daily_images_to_delete))
    delete_images(rice_daily_images_to_delete, rice_repo)

    # ----------------- RICE RELEASE IMAGES ---------------   

    rice_release_builds = get_release_build_list(rice_repo)
    rice_release_images_to_delete = release_age_search(rice_release_builds)
    print("rice_release_images_to_delete: ", len(rice_release_images_to_delete))
    delete_images(rice_release_images_to_delete, rice_repo)

if __name__ == '__main__':
  main()
