#!/bin/bash -e

# Create a custodian directory
mkdir -p custodian
# pull our policies from S3 and put in that directory
aws s3 cp s3://kfs-cloud-custodian/policies custodian --recursive

# If we have more than one account, iterate through the list of accounts.
account_list=( 397167497055 )
for i in "${account_list[@]}"
do
    :
    printf '\nProcessing for account: '$i'\n'
    cache_path=~/.cache/custodian.${i}.cache
    echo 'Cache path: '$cache_path
    # this is the S3 bucket/folder we will store our run logs in.
    s3_path=s3://kfs-cloud-custodian/run_logs/${i}
    echo 'Output path: '$s3_path
    role_arn=arn:aws:iam::${i}:role/cloudcustodian-instance-role
    echo 'Assume role ARN: '$role_arn

    # regsion HAS to be us-east-1 for now because opsworks only existed in east
    region_id="us-east-1"
    # We can run more than one policy at a time.  Add more filenames to the policy_lists if needed separated by a space
    # Example: policy_list=( ec2-long-running.yml s3-global-grants.yml )
    policy_list=( opsworks_stop.yml )
    for p in "${policy_list[@]}"
    do
        :
        policy_file=/home/ec2-user/custodian/${p}
        printf '\nProcessing policy: '$policy_file'...\n'
        # run the custodian command
        custodian run --cache-period=15 --cache $cache_path -s $s3_path --region=$region_id --assume=$role_arn -c $policy_file -m
    done
done
