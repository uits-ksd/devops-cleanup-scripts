# ARCHIVED - NO LONGER USED

# UAccess Financials DevOps Cleanup Scripts
---

This repository is for the Kuali DevOps scripts that are used to clean up artifacts.

## Scripts

### nexus_cleanup.sh
Adapted from the nexus_clean.sh script on GitHub:

- Repository: https://github.com/akquinet/nexus_cleaner
- Related documentation: https://blog.akquinet.de/2013/12/09/how-to-clean-your-nexus-release-repositories

This script assumes you can run scripts on the command line and have the permissions to delete files. The script takes five command line arguments:

1. `dry_run_flag`: If `Y`, will print out commands that will run to remove directories, as opposed to running them.
1. `path`: Full path of repository. Example: /uaccess/KATTS/Internal/UAF/tools/nexus/nexus-data/storage
1. `library`: The directory on the file system that represents a Repository in the Nexus Repository Manager. Example: snapshots
1. `group_id`: The Group listed in the artifact's metadata, which also matches part of the path to the artifact. Example: org.kuali.kfs
1. `keep_versions`: The number of versions to retain; corresponds to artifact directories.

Example command to run: `sh nexus_cleanup.sh N /uaccess/KATTS/Internal/UAF/tools/nexus/nexus-data/storage snapshots-hlo org.kuali.kfs 5`

### run_cleanup_commands.sh
Runs "rm -rf" commands contained in an input file. Written because our Rice versioning changed throughout the KFS upgrade, and the nexus_cleanup.sh script does not sort by modified date.

The script requires a file with "rm -rf" commands, such as one created by running `sh devops-cleanup-scripts/scripts/nexus_cleanup.sh Y /uaccess/KATTS/Internal/UAF/tools/nexus/nexus-data/storage snapshots org.kuali.rice 0 > rice_snapshots-full_list.txt` and then modified to remove the commands that reflect the versions we want to keep.

Example command to run: `sh run_cleanup_commands.sh rice_snapshots-modified_list.txt`


### ECS_image_clean.py
This script is using the AWS SDK for python which is used for connecting to AWS services. See: https://aws.amazon.com/sdk-for-python/ for more details

This script is currently (6/25/18) run in Jenkins at 12AM every morning.  No reason for 12AM, so that can be changed if needed.
https://kuali-jenkins.ua-uits-kuali-nonprod.arizona.edu/jenkins/job/Development/job/ECR%20-%20KFS%20Docker%20image%20cleanup/

The script does the following

1. `DRYRUN` is passed in as a parameter from Jenkins, and is a flag that turns on or off the actual deleting.  `DRYRUN` = False will make the script delete.   We could make it a param in the Jenkins job if needed, but since this is more of a set and forget script, it should be fine now.
1. First the script gets a list of docker images that are considered 'daily build' images.
1. Next the script then passes that list into the age search which has a requirement of 'get images that are older than 'x' days'
1. Then it deletes those images if `DRYRUN` is False
1. Second, it gets a list of images that have 'release' tags in them
1. Then it will pass that list to the age search, which for release is 365 days, and delete any 'release build' over 365 days old.
1. NOTE: If DRYRUN = True, then it will print out a list of images that will be deleted, but not actually delete them, or do anything other than print the info.

### custodian_opsworks_stop.sh
This script is used to run Cloud Custodian policies.  At this time, it is stored here as a backup.

This script is currently (7/24/18) run in Jenkins at 8PM every night.
https://kuali-jenkins.ua-uits-kuali-nonprod.arizona.edu/jenkins/job/Development/job/Cloud%20Custodian%20-%20Stop%20Prototype%20instances%20daily/